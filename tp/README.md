# TP

* [TP1 : Déploiement classique](./1/README.md)
* [TP2 : Déploiement automatisé](./2/README.md)

## Rendu de TP 

Le rendu de TP **doit** se faire *via* un dépôt git, au format Markdown.

TOUTES les opérations pour réaliser ce qui est demandé doivent figurer dans le rendu de TP.

L'OS GNU/Linux utilisé pour les TPs sera CentOS7.

Les points précédés d'un 🌞 doivent **obligatoirement** figurer dans le rendu.  
Les points précédés d'un 🐙 sont des bonus, ils sont facultatifs.

Le rendu de TP se fait *via* Discord, en m'envoyant un message privé au format `nom,url_du_dépôt_git`.  
Par exemple : `macron,https://gitlab.inria.fr/stopcovid19/accueil`.

**Un rendu par TP et par personne.**
