# TP1 : Déploiement classique

<!-- vim-markdown-toc GitLab -->

* [0. Prérequis](#0-prérequis)
    * [Partitionnement](#partitionnement)
    * [Réseau](#réseau)
    * [SELinux](#selinux)
* [Vérif](#vérif)
* [I. Setup serveur Web](#i-setup-serveur-web)
* [II. Script de sauvegarde](#ii-script-de-sauvegarde)
* [III. Monitoring, alerting](#iii-monitoring-alerting)

<!-- vim-markdown-toc -->

# 0. Prérequis
# TP1 : Déploiement classique

<!-- vim-markdown-toc GitLab -->

* [0. Prérequis](#0-prérequis)
    * [Partitionnement](#partitionnement)
    * [Réseau](#réseau)
    * [SELinux](#selinux)
* [Vérif](#vérif)
* [I. Setup serveur Web](#i-setup-serveur-web)

<!-- vim-markdown-toc -->

# 0. Prérequis

## Partitionnement

```bash
[it4@node1 ~]$ lsblk
[it4@node1 ~]$ sudo pvcreate /dev/sdb
[it4@node1 ~]$ sudo vgcreate site /dev/sdb
[it4@node1 ~]$ sudo lvcreate -L 3G site -n site1
[it4@node1 ~]$ sudo lvcreate -l 100%FREE site -n site2
[it4@node1 ~]$ sudo mkfs.ext4 /dev/site/site1
[it4@node1 ~]$ sudo mkfs.ext4 /dev/site/site2
[it4@node1 ~]$ sudo mkdir /srv/site{1,2}
[it4@node1 ~]$ sudo vim /etc/fstab
[it4@node1 ~]$ sudo mount -av
[...]
/srv/site1               : successfully mounted
/srv/site2               : successfully mounted
```

Vérification
```bash
[it4@node1 ~]$ cat /etc/fstab
[...]
/dev/data/data1 /srv/site1 ext4 defaults 0 0
/dev/data/data2 /srv/site2 ext4 defaults 0 0

[it4@node1 ~]$ mount
[...]
/dev/mapper/data-data2 on /srv/site2 type ext4 (rw,relatime,seclabel,data=ordered)
/dev/mapper/data-data1 on /srv/site1 type ext4 (rw,relatime,seclabel,data=ordered)

[it4@node1 ~]$ df -h
Filesystem               Size  Used Avail Use% Mounted on
[...]
/dev/mapper/data-data2   2.0G  6.1M  1.9G   1% /srv/site2
/dev/mapper/data-data1   2.9G  9.1M  2.8G   1% /srv/site1
```

## Réseau

Cartes réseau
```bash
[it4@node1 ~]$ ip a show enp0s3 # carte permettant un accès externe
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:25:94:0b brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 84560sec preferred_lft 84560sec
    inet6 fe80::a3d4:6943:55e7:d7da/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
[it4@node1 ~]$ ip a show enp0s8 # carte permettant un accès à un réseau local
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:4d:9b:ad brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.11/24 brd 192.168.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe4d:9bad/64 scope link 
       valid_lft forever preferred_lft forever
```

Routes
```bash
[it4@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100 # route vers internet
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100 
192.168.1.0/24 dev enp0s8 proto kernel scope link src 192.168.1.11 metric 101 # route réseau locale
```

Hostname
```bash
[it4@node1 ~]$ sudo vim /etc/hostname
[it4@node1 ~]$ cat /etc/hostname
node1.tp1.b2
[it4@node1 ~]$ sudo hostname $(cat /etc/hostname)

# Vérif
[it4@node1 ~]$ hostname
node1.tp1.b2
```

Fichier `/etc/hosts`
```bash
[it4@node1 ~]$ sudo vim /etc/hosts
[it4@node1 ~]$ cat /etc/hosts
[...]
192.168.1.11 node1 node1.tp1.b2
192.168.1.12 node2 node2.tp1.b2


# Vérif
[it4@node1 ~]$ ping -c 1 node1
PING node1 (192.168.1.11) 56(84) bytes of data.
64 bytes from node1 (192.168.1.11): icmp_seq=1 ttl=64 time=0.105 ms

--- node1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.105/0.105/0.105/0.000 ms

[it4@node1 ~]$ ping -c 1 node2
PING node2 (192.168.1.12) 56(84) bytes of data.
64 bytes from node2 (192.168.1.12): icmp_seq=1 ttl=64 time=3.00 ms

--- node2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 3.000/3.000/3.000/0.000 ms
```

SSH
```bash
# Déposer la clé publique sur l'utilisateur distant
[it4@nowhere suggestion_correction]$ ssh-copy-id 192.168.1.11
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 2 key(s) remain to be installed -- if you are prompted now it is to install the new keys
it4@192.168.1.11's password: 

Number of key(s) added: 2

Now try logging into the machine, with:   "ssh '192.168.1.11'"
and check to make sure that only the key(s) you wanted were added.
```

```bash
# Vérif
[it4@nowhere suggestion_correction]$ ssh 192.168.1.11
Last login: Mon Sep 28 18:12:40 2020 from 192.168.1.1

[it4@node1 ~]$ hostname
node1.tp1.b2
```

Firewall
```bash
# Le pare-feu est déjà restrictif
[it4@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules:
```
## SELinux

```bash
# Désactivation
[it4@node1 ~]$ sudo setenforce 0
[it4@node1 ~]$ sudo vim /etc/selinux/config
[it4@node1 ~]$ sudo cat /etc/selinux/config
[...]
SELINUX=permissive
[...]

# Vérif
[it4@node1 ~]$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   permissive
Mode from config file:          permissive
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Max kernel policy version:      31
```

## Utilisateur

 ```bash
[it4@node1 ~]$ sudo useradd admin -m
[it4@node1 ~]$ sudo usermod -aG wheel admin

# Vérif
[it4@node1 ~]$ sudo ls /root
anaconda-ks.cfg
 ```

# I. Setup serveur Web

```bash
# Install serveur
[it4@node1 ~]$ sudo yum install -y epel-release
[it4@node1 ~]$ sudo yum install -y nginx

# Création d'un user dédié pour NGINX
[it4@node1 ~]$ sudo useradd web -M -s /sbin/nologin

# Génération clés + certificat
[it4@node1 ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt

# Déplacement du cert et de la clé dans le path standard sous CentOS
[it4@node1 ~]$ sudo mv ~/server.key /etc/pki/tls/private/node1.tp1.b2.key
[it4@node1 ~]$ sudo chmod 400 /etc/pki/tls/private/node1.tp1.b2.key
[it4@node1 ~]$ sudo chown web:web /etc/pki/tls/private/node1.tp1.b2.key

[it4@node1 ~]$ sudo mv ~/server.crt /etc/pki/tls/certs/node1.tp1.b2.crt
[it4@node1 ~]$ sudo chown web:web /etc/pki/tls/certs/node1.tp1.b2.crt
[it4@node1 ~]$ sudo chmod 444 /etc/pki/tls/certs/node1.tp1.b2.crt

# Setup des deux sites web
[it4@node1 ~]$ echo '<h1>Hello from site 1</h1>' | tee /srv/site1/index.html
[it4@node1 ~]$ echo '<h1>Hello from site 2</h1>' | tee /srv/site2/index.html
[it4@node1 ~]$ sudo chown web:web /srv/site1 -R
[it4@node1 ~]$ sudo chmod 700 /srv/site1 /srv/site2
[it4@node1 ~]$ sudo chmod 400 /srv/site1/index.html /srv/site2/index.html

# Config de NGINX
[it4@node1 ~]$ sudo vim /etc/nginx/nginx.conf
[it4@node1 ~]$ cat /etc/nginx/nginx.conf
worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;

events {
    worker_connections 1024;
}

http {
    server {
        listen 80;
        server_name node1.tp1.b2;
        
        location / {
              return 301 /site1;
        }

        location /site1 {
            alias /srv/site1;
        }

        location /site2 {
            alias /srv/site2;
        }
    }
    server {
        listen 443 ssl;

        server_name node1.tp1.b2;
        ssl_certificate /etc/pki/tls/certs/node1.tp1.b2.crt;
        ssl_certificate_key /etc/pki/tls/private/node1.tp1.b2.key;
        
        location / {
              return 301 /site1;
        }

        location /site1 {
            alias /srv/site1;
        }

        location /site2 {
            alias /srv/site2;
        }
    }
}
```

# II. Script de sauvegarde

Gestion de permissions :

```bash
[it4@node1 ~]$ ls -al /opt/
total 4
drwxr-xr-x.  3 root   root     41 Sep 30 14:30 .
dr-xr-xr-x. 17 root   root    224 Mar  8  2020 ..
drwx------.  2 backup backup  244 Sep 30 14:32 backup
-r-x------.  1 backup backup 3581 Sep 30 14:30 tp1_backup.sh

[it4@node1 ~]$ groups backup
backup : backup web

[it4@node1 ~]$ ls -al /srv
total 8
drwxr-xr-x.  4 root   root     32 Sep 23 15:32 .
dr-xr-xr-x. 17 root   root    224 Mar  8  2020 ..
dr-xr-x---.  3 web    web    4096 Sep 23 15:35 site1
dr-xr-x---.  3 web    web    4096 Sep 23 15:35 site2
```

Script dans `/opt/tp1_backup.sh` :
```bash
#!/bin/bash
# it4
# 23/09/2020
# Simple backup script

## VARIABLES

# Colors :D
NC="\e[0m"
B="\e[1m"
RED="\e[31m"
GRE="\e[32m"

# Target directory : the one we want to backup
declare -r target_path="${1}"
declare -r target_dirname=$(awk -F'/' '{ print $NF }' <<< "${target_path%/}")

# Craft the backup full path and name
declare -r backup_destination_dir="/opt/backup/"
declare -r backup_date="$(date +%y%m%d_%H%M%S)"
declare -r backup_filename="${target_dirname}_${backup_date}.tar.gz"
declare -r backup_destination_path="${backup_destination_dir}/${backup_filename}"

# Informations about the User that must run this script
declare -r backup_user_name="backup"
declare -ri backup_user_uid=1003
declare -ri backup_user_umask=077

# The quantity of backup to keep for each directory
declare -i backups_quantity=7
declare -ri backups_quantity=$((backups_quantity+1))

## FUNCTIONS

# Get timestamp in order to log
get_current_timestamp() {
  timestamp=$(date "+[%h %d %H:%M:%S]")
}

# Echo arguments with a timestamp
log() {
  log_level="${1}"
  log_message="${2}"

  get_current_timestamp

  if [[ "${log_level}" == "ERROR" ]]; then
    echo -e "${timestamp} ${B}${RED}[ERROR]${NC} ${log_message}" >&2

  elif [[ "${log_level}" == "INFO" ]]; then
    echo -e "${timestamp} ${B}[INFO]${NC} ${log_message}"

  fi
}

# Craft an archive, compress it, and store it in $backup_destination_dir
archive_and_compress() {

  dir_to_backup="${1}"

  log "INFO" "Starting backup."

  # Actually creates the compressed archive
  tar cvzf \
    "${backup_destination_path}" \
    "${dir_to_backup}" \
    --ignore-failed-read &> /dev/null

  # Test if the archive has been created successfully
  if [[ $? -eq 0 ]]
  then
    log "INFO" "${B}${GRE}Success.${NC} Backup ${backup_filename} has been saved to ${backup_destination_dir}."
  else 
    log "ERROR" "Backup ${backup_filename} has failed."

    # Even if tar has failed, it creates the archive, so we remove it in case of failure
    rm -f "${backup_destination_path}"

    exit 1
  fi
}

# Delete oldest backups, eg only keep the $backups_quantity most recent backups, for a given directory
delete_oldest_backups() {

  # Get list of oldest backups
  # BE CAREFUL : this only works if there's no IFS character in file names (space, tabs, newlines, etc.)
  oldest_backups=$(ls -tp "${backup_destination_dir}" | grep -v '/$' | grep -E "^${target_dirname}.*$" | tail -n +${backups_quantity})

  if [[ ! -z $oldest_backups ]]
  then

    log "INFO" "This script only keep the ${backups_quantity} most recent backups for a given directory."

    for backup_to_del in ${oldest_backups}
    do
      # This line might be buggy if file names contain IFS characters 
      rm -f "${backup_destination_dir}/${backup_to_del}" &> /dev/null

      if [[ $? -eq 0 ]]
      then
        log "INFO" "${B}${GRE}Success.${NC} Backup ${backup_to_del} has been removed from ${backup_destination_dir}."
      else
        log "[ERROR]" "Deletion of backup ${backup_to_del} from ${backup_destination_dir} has failed."
        exit 1
      fi

    done
  fi
}

## PREFLIGHT CHECKS

# Force a specific user to run te script
if [[ ${EUID} -ne ${backup_user_uid} ]]; then
  log "ERROR" "This script must be run as \"${backup_user_name}\" user, which UID is ${backup_user_uid}. Exiting."
  exit 1
fi

# Check that the target dir actually exists and is readable
if [[ ! -d "${target_path}" ]]; then
  log "ERROR" "The target path ${target_path} does not exist. Exiting."
  exit 1
fi
if [[ ! -r "${target_path}" ]]; then
  log "ERROR" "The target path ${target_path} is not readable. Exiting."
  exit 1
fi

# Check that the destination dir actually exists ans is writable
if [[ ! -d "${backup_destination_dir}" ]]; then
  log "ERROR" "The destination dir ${backup_destination_dir} does not exist. Exiting."
  exit 1
fi
if [[ ! -w "${backup_destination_dir}" ]]; then
  log "ERROR" "The destination dir ${backup_destination_dir} is not writable. Exiting."
  exit 1
fi


### CODE

# Set the backup user UMASK
umask ${backup_user_umask}

# Backup the site
archive_and_compress "${target_path}"

# Rotate backups (only keep the most recent ones)
delete_oldest_backups
```

Crontab :
```bash
[it4@node1 ~]$ cat /etc/crontab 
[...]
# Sauvegarde de /srv/site1 toutes les heures
30 * * * * backup /opt/tp1_backup.sh /srv/site1
# Sauvegarde de /srv/site2 toutes les heures
30 * * * * backup /opt/tp1_backup.sh /srv/site2
```

Procédure de restauration. On admet qu'on veut restaurer la backup `site1_200930_153207.tar.gz`, pour le site `site2` :
```bash

# Récupération de la sauvegarde voulue
[it4@node1 ~]$ sudo ls -al /opt/backup/

# Suppression de l'ancienne version
[it4@node1 ~]$ sudo rm -rf /srv/site2/*

# On s'assure que ça a bien été removed
[it4@node1 ~]$ sudo ls -al /srv/site2

# Restauration
[it4@node1 ~]$ sudo tar xvzf site1_200930_153207.tar.gz -C /srv/site2
```

Bonus unité systemd :
```bash
[it4@node1 anchors]$ sudo systemctl cat backup
# /etc/systemd/system/backup.service
[Unit]
Description=B1 TP1 Backup script

[Service]
User=backup
Group=backup
UMask=277

Type=oneshot
#RemainAfterExit=yes
ExecStart=/opt/tp1_backup.sh /srv/site1
ExecStart=/opt/tp1_backup.sh /srv/site2

[Install]
WantedBy=multi-user.target
```

Bonus-in-bonus timer systemd :
```bash
[it4@node1 anchors]$ sudo systemctl cat backup.timer
# /etc/systemd/system/backup.timer
[Unit]
Description=Run backup every hour

[Timer]
OnCalendar=hourly

[Install]
WantedBy=timers.target
```
