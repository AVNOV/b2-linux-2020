# Cours

* [Mémos](./memo)
  * [Mémo des commandes et fichiers élémentaires du système](./memo/basics.md)
  * [Mémo partitionnement, LVM](./memo/part.md)
  * [Mémo réseau CentOS 7](./memo/centos_network.md)


